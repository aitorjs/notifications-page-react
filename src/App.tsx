import { useState } from 'react'
import './App.css'

function App() {
  const [counter, setCounter] = useState(3)
  const [isRead, setIsRead] = useState([false, false, false, true, true, true, true])


  const handleClick = (index: number) => {
    isRead[index] = !isRead[index]
    setIsRead(isRead)
    isRead[index] ? setCounter(counter - 1) : setCounter(counter + 1)
  }

  const handleAllRead = () => {
    setIsRead([true, true, true, true, true, true, true])
    setCounter(0)
  }

  const setNotificationClassName = (index: number) => {
    return isRead[index] ? 'notification' : 'notification unread'
  }
  return (
    <main>
      <header>
        <span>
          <span>Notifications </span>
          <span>{counter}</span>
        </span>

        <span className={counter === 0 ? 'opacity' : 'block'} onClick={() => handleAllRead()}>Mark all as read</span>
      </header>

      <section className="notifications">
        <div className={setNotificationClassName(0)} onClick={() => handleClick(0)}>
          <img src="./assets/images/avatar-mark-webber.webp" alt="Mark Webber's avatar" />
          <div>
            <h3>
              <span className="notification_username">Mark Webber</span> reacted to your recent post
              <span className="notification_text">My first tournament today!</span>
              <span className="offline">&nbsp;</span>
            </h3>
            <span>1m ago</span>
          </div>
        </div>

        <div className={setNotificationClassName(1)} onClick={() => handleClick(1)}>
          <img src="./assets/images/avatar-angela-gray.webp" alt="Angela Gray's avatar" />
          <div>
            <h3>
              <span className="notification_username">Angela Gray</span> followed you
              <span className="offline">&nbsp;</span>
            </h3>
            <span>5m ago</span>
          </div>
        </div>

        <div className={setNotificationClassName(2)} onClick={() => handleClick(2)}>
          <img src="./assets/images/avatar-jacob-thompson.webp" alt="Jacob Thompson's avatar" />
          <div>
            <h3>
              <span className="notification_username">Jacob Thompson</span> has joined your group
              <span className="your_group_name">Chess Club</span>
              <span className="offline">&nbsp;</span>
            </h3>
            <span>1 day ago</span>
          </div>
        </div>

        <div className={setNotificationClassName(3)} onClick={() => handleClick(3)}>
          <img src="./assets/images/avatar-rizky-hasanuddin.webp" alt="Rizky Hasanuddin's avatar" />
          <div>
            <h3>
              <span className="notification_username">Rizky Hasanuddin</span> sent you a private message
            </h3>
            <span>5 days ago</span>
            <div className="notification_message">
              Hello, thanks for setting up the Chess Club. I've been a member for a few weeks now and
              I'm already having lots of fun and improving my game.
            </div>
          </div>
        </div>

        <div className={isRead[4] ? 'notification no-margin' : 'notification no-margin unread'} onClick={() => handleClick(4)}>
          <img src="./assets/images/avatar-kimberly-smith.webp" alt="Kimberly Smith's avatar" />
          <div>
            <h3>
              <span className="notification_username">Kimberly Smith</span> commented on your picture
            </h3>
            <span>1 week ago</span>
          </div>
          <img className="my_image" src="./assets/images/image-chess.webp" alt="Chess image" />
        </div>

        <div className={setNotificationClassName(5)} onClick={() => handleClick(5)}>
          <img src="./assets/images/avatar-nathan-peterson.webp" alt="Nathan Peterson's avatar" />
          <div>
            <h3>
              <span className="notification_username">Nathan Peterson</span> reacted to your recent post
              <span className="notification_text">5 end-game strategies to increase your win rate</span>
            </h3>
            <span>2 weeks ago</span>
          </div>
        </div>

        <div className={setNotificationClassName(6)} onClick={() => handleClick(6)}>
          <img src="./assets/images/avatar-anna-kim.webp" alt="Anna Kim's avatar" />
          <div>
            <h3>
              <span className="notification_username">Anna Kim</span> left the group
              <span className="your_group_name">Chees group</span>
            </h3>
            <span>2 weeks ago</span>
          </div>
        </div>
      </section>
    </main>
  )
}

export default App
